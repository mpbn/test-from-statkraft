require([
    "esri/map",
    "esri/geometry/Extent",
    "esri/layers/FeatureLayer",
    "esri/renderers/UniqueValueRenderer"
],
    
    function (
        Map,
        Extent,
        FeatureLayer,
        UniqueValueRenderer
    ) {
        var map = new Map("myMap", {
            basemap: "osm"
            });
    
        var hydroProduction = new FeatureLayer("https://gis3.nve.no/map/rest/services/Vannkraft/MapServer/0", {
                id: "Production"
            });
    
    var rendererJson = {
            "type": "uniqueValue",
            "field1": "KraftverkSymbol",
            "defaultSymbol": {},
            "uniqueValueInfos": [{
                "value": "K",
                "symbol": {
                    "color": [255, 0, 0, 191],
                    "size": 6,
                    "type": "esriSMS",
                    "style": "esriSMSCircle"
                }
            }, {
                "value": "PK",
                "symbol": {
                    "color": [0, 0, 255, 191],
                    "size": 6,
                    "type": "esriSMS",
                    "style": "esriSMSCircle"
                }
            }, {
                "value": "P",
                "symbol": {
                    "color": [0, 255, 0, 191],
                    "size": 6,
                    "type": "esriSMS",
                    "style": "esriSMSCircle"
                }
            }, {
                "value": "SK",
                "symbol": {
                    "color": [125, 0, 125, 191],
                    "size": 6,
                    "type": "esriSMS",
                    "style": "esriSMSCircle"
                }
            }]
        };
        var renderer = new UniqueValueRenderer(rendererJson);
           
        hydroProduction.setRenderer(renderer);

    
    map.addLayer(hydroProduction);
    
});